const express = require('express')
const app = express()
app.use(express.json());
var port = process.env.PORT || 8081;
const cors = require('cors') //New for Microservice
app.use(cors()) //New for Microservice
const MongoClient = require('mongodb').MongoClient;
const mongourl = "mongodb+srv://cca-vadlamudiv1:Viswa.4599@cca-vadlamudiv1.c6jmd.mongodb.net/cca-labs?retryWrites=true&w=majority";
const dbClient = new MongoClient(mongourl, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});
dbClient.connect(err => {
    if (err) throw err;
    console.log("Connected to MongoDB Cluster")
})
let fields = {
    _id: false,
    zips: true,
    city: true,
    state_id: true,
    state_name: true,
    county_name: true,
    timezone: true
};
app.use(express.static('static'))
app.use(express.urlencoded({
    extended: false
}))
app.listen(port, () =>
    console.log('HTTP Server with Express.js is listening on port: ' + port))
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/static/cca-form.html');
})
app.get('/echo.php', function (req, res) {
    var data = req.query.data
    res.send(data)
})
app.post('/echo.php', function (req, res) {
    var data = req.body['data']
    res.send(data)
})
app.get("/uscities-search", (req, res) => {
    res.send("US City Microservice by Viswanath Vadlamudi")
})
app.get('/uscities-search/:zips(\\d{1,5})', function (req, res) {
    const db = dbClient.db();
    let zipRegEx = new RegExp(req.params.zips);
    const cursor = db.collection("uscities").find({
        zips: zipRegEx
    }).project(fields);
    cursor.toArray(function (err, results) {
        console.log(results);
        res.send(results);
    });
});
app.get('/uscities-search/:city', (req, res) => {
    const db = dbClient.db();
    let cityRegEx = new RegExp(req.params.city, 'i');
    const cursor = db.collection("uscities").find({
        city: cityRegEx
    }).project(fields);
    cursor.toArray(function (err, results) {
        console.log(results);
        res.send(results);
    })
})

app.post('/signup', (req, res) => {

    const db = dbClient.db();

    username = req.body.username
    password = req.body.password
    fullname = req.body.fullname
    email = req.body.email

    db.collection("users").findOne({
        username : username
    }, (err, user) => {
        if (user) {
            res.send({code : 201 , message: "user already exists"})
        } else {
            let newUser = {
                username: username,
                password: password,
                fullname : fullname,
                email : email
            };
            db.collection("users").insertOne(newUser, (err, result) => {
                if (err) {
                    console.log(err)
                    res.send({code : 400 , message : "error creating user"})
                }
                res.send({code : 200,  message: "created successfully"})
            });
        }
    });

})


app.post('/login', (req, res) => {

    const db = dbClient.db();

    username = req.body.username
    password = req.body.password
    
    db.collection("users").findOne({
        username : username, password : password
    }, (err, user) => {
        if (user) {
            res.send({code : 200 , message: "user logged in"})
        } else {
            res.send({code: 400, message: "login failed"})
        }
    });

})